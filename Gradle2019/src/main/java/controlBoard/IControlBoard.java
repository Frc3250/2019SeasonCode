package controlBoard;

import coordinates.Heading;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public abstract class IControlBoard{
    public abstract Heading getJoystickPos();
    public abstract double getWheelPos();
    public abstract Heading getCoJoyPos();
    public abstract double getCoJoySlider();
    public abstract Joystick getPathsJoystick();
    public abstract double armLength();
    public abstract boolean slowDrive();
    public abstract boolean visionDrive();
    public abstract boolean visionDrivePressed();
    public abstract boolean visionLED();

    public abstract boolean autoStop();

    public abstract boolean climbMode();

    public abstract boolean resetTelescope();
    public abstract boolean disableTelescopeGripper();

    public abstract boolean quickTurn();

    public abstract boolean gripperGrab();

    public abstract boolean flipArm();
    public abstract boolean armToInside();
    public abstract boolean armToBallPickup();
    public abstract boolean armToBallGoal();
    public abstract boolean armToHatchPickup();
    public abstract boolean armToHatchSecondLevel();
    public abstract boolean armToHatchThirdLevel();
    public abstract boolean incrementOffset();
    public abstract boolean decrementOffset();

    public abstract boolean gripperShoot();
    public abstract boolean gripperShootPressed();

    public abstract boolean lowClimb();
    public abstract boolean climbUp();
    public abstract boolean climbForward();
    public abstract boolean climbRetract();

    //TODO: maybe remove this
    public abstract boolean ballPistonGrab();

    public abstract boolean cargoPivot();
    public abstract boolean cargoGrab();
    public abstract boolean cargoShoot();

    public void display(){
        SmartDashboard.putString("Co-joystick pos", getCoJoyPos().display());
        SmartDashboard.putBoolean("Arm to inside", armToInside());
        SmartDashboard.putBoolean("Arm to hatch pickup", armToHatchPickup());
        SmartDashboard.putBoolean("Arm to hatch second", armToHatchSecondLevel());
        SmartDashboard.putBoolean("Arm to hatch third", armToHatchThirdLevel());
    }
}
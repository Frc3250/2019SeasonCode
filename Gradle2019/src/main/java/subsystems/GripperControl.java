package subsystems;

import edu.wpi.first.wpilibj.RobotState;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import robot.Constants;
import robot.Robot;
import utilPackage.Units;
import utilPackage.Util;

public class GripperControl{
    private static GripperControl instance;
    public static GripperControl getInstance(){
        if(instance == null){
            instance = new GripperControl();
        }
        return instance;
    }

    public enum States{
        disabled,
        running;
    }

    States state;
    Gripper gripper;
    double setpoint = 0;

    double resetSetpoint = 0;

    boolean disableReset = false;

    private GripperControl(){
        state = States.disabled;
        gripper = Gripper.getInstance();
    }

    public void setSetpoint(double setpoint){
        SmartDashboard.putNumber("Gripper setpoint", setpoint/Units.Angle.degrees);
        setpoint = Util.forceInRange(setpoint, Constants.Gripper.minAngle, Constants.Gripper.maxAngle);
        this.setpoint = setpoint;
    }

    public void run(){
        switch(state){
            case disabled:
                if(RobotState.isEnabled() && TelescopeControl.getInstance().isRunning()){
                    state = States.running;
                }
                break;
            case running:
                if(Robot.getControlBoard().disableTelescopeGripper()){
                    gripper.setVoltage(0);
                    return;
                }
                double feedforward = gripper.getAntigrav();
                double p = 22.6440;
                double d = 0.4937;
                double error;
                if(MainArm.getInstance().getAngle() < Constants.MainArm.insideAngle){
                    // if(Robot.getControlBoard().isCargoMode()){
                    //     error = 35*Units.Angle.degrees - gripper.getRelAngle();
                    // }else{
                        error = Math.max(Constants.Gripper.maxAngle, setpoint) - gripper.getRelAngle();
                    // }
                }else{
                    error = setpoint - gripper.getRelAngle();
                }
                double dError = -gripper.getRelAngleVel();
                double feedback = p*error + d*dError;
                // gripper.setVoltage(feedforward);
                gripper.setVoltage(feedforward+feedback);
                break;
        }
    }

    public boolean isRunning(){
        return state == States.running;
    }
}